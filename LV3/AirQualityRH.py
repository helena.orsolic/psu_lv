import urllib.request as ur
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

#1. Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek. 
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = ur.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = list(root)[i]
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = (df['vrijeme'].dt.dayofweek) + 1
df['day'] = df['vrijeme'].dt.day
df.plot(y='mjerenje', x='vrijeme') 

#2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća. 
print(df.sort_values(['mjerenje']).tail(3))

#3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca
zabiljezeniDani = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
for a in df.month:
    zabiljezeniDani[a-1]=zabiljezeniDani[a-1]+1
print(zabiljezeniDani)
propusteniDani = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
i=0
for a in zabiljezeniDani:
    if((a<30) & (a!=28)):
        propusteniDani[i] = 31 - a
    i=i+1
print(propusteniDani)
plt.figure(2)
plt.bar([1,2,3,4,5,6,7,8,9,10,11,12],propusteniDani)
 
 
#4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
new_df = df[(df.month==1) | (df.month==7)]
new_df.boxplot(column=['mjerenje'],by='month')
 

#5. Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda.
radniDan = np.zeros(len(df), dtype=int)
i=0
for a in df.dayOfweek:
    if(a <= 5):
        radniDan[i] = 1
    elif(a > 5):
        radniDan[i] = 2
    i=i+1

df['radniDan'] = radniDan
df.boxplot(column=['mjerenje'],by='radniDan')

plt.show()