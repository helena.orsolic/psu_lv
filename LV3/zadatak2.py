import pandas as pd
import matplotlib.pyplot as plt


mtcars = pd.read_csv('mtcars.csv')

#Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
new_mtcars = mtcars.groupby('cyl').mpg.mean().plot.bar()
plt.bar(mtcars.cyl, mtcars.mpg) #drugi nacin

#Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara
new1_mtcars=mtcars.boxplot(column=['wt'],by='cyl')

#Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem 
# veću potrošnju od automobila s automatskim mjenjačem
new2_mtcars=mtcars.boxplot(column=['mpg'],by='am')

#Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno 
# automatskim mjenjačem.
color=mtcars.am
plt.figure(4)
plt.scatter(mtcars.qsec,mtcars.hp, c=color)
plt.show()

