import pandas as pd

mtcars = pd.read_csv('mtcars.csv')

#Kojih 5 automobila ima najveću potrošnju?
print(mtcars.sort_values(['mpg']).head(5)) 

#Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
print(mtcars[mtcars.cyl==8].sort_values(['mpg']).tail(3))  

#Kolika je srednja potrošnja automobila sa 6 cilindara (19.74)
print(mtcars[mtcars.cyl==6].mpg.mean()) 

#Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs? (29.2)
print(mtcars[(mtcars.cyl==4) & (mtcars.wt>=2.0) & (mtcars.wt<=2.2)].mpg.mean()) 

#Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka? (13 rucnih i 19 automatskih)
print(len(mtcars) - mtcars.am.sum()) 

#Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga? (16)
print(len(mtcars[(mtcars.am==0) & (mtcars.hp>=100)])) 

#Kolika je masa svakog automobila u kilogramima?
mtcars['kg'] = mtcars.wt *  0.453 * 1000
print(mtcars) 

