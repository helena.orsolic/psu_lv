import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# ucitavanje ociscenih podataka
df = pd.read_csv('cars_processed.csv')
print(df.info())
print(df.sort_values(['selling_price']).head(1)) 
print(df.sort_values(['selling_price']).tail(1)) 
print(len(df[df.year==2012]))  #575
print(df.sort_values(['km_driven']).head(1)) 
print(df.sort_values(['km_driven']).tail(1))


sns.pairplot(df, hue='fuel')

sns.relplot(data=df, x='km_driven', y='selling_price', hue='fuel')
df = df.drop(['name','mileage'], axis=1)

obj_cols = df.select_dtypes(object).columns.values.tolist()
num_cols = df.select_dtypes(np.number).columns.values.tolist()

fig = plt.figure(figsize=[15,8])
for col in range(len(obj_cols)):
    plt.subplot(2,2,col+1)
    sns.countplot(x=obj_cols[col], data=df)

df.boxplot(by ='fuel', column =['selling_price'], grid = False)

df.hist(['selling_price'], grid = False)

tabcorr = df.corr()
sns.heatmap(df.corr(), annot=True, linewidths=2, cmap= 'coolwarm') 


#               name  year  selling_price  km_driven    fuel seller_type transmission        owner  mileage  engine  max_power  seats
#4778  Maruti 800 AC  1997      10.308919      80000  Petrol  Individual       Manual  Third Owner     16.1     796       37.0      4

#                       name  year  selling_price  km_driven    fuel seller_type transmission        owner  mileage  engine  max_power  seats
#2591  BMW X7 xDrive 30d DPE  2020      15.789592       5000  Diesel  Individual    Automatic  First Owner    13.38    2993      265.0      7


#                                        name  year  selling_price  km_driven fuel seller_type transmission                 owner  mileage  engine  max_power  seats
#6514  Maruti Eeco 5 STR With AC Plus HTR CNG  2011       12.25009          1  CNG  Individual       Manual  Fourth & Above Owner     15.1    1196       73.0      5
#                          name  year  selling_price  km_driven    fuel seller_type transmission         owner  mileage  engine  max_power  seats
#3067  Maruti Wagon R LXI Minor  2010      12.175613     577414  Petrol  Individual       Manual  Second Owner     18.9    1061       67.0      5