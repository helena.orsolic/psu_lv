import matplotlib.pyplot as plt
import numpy as np
from sympy import Range

# create a n * n matrix
mat = np.zeros((4,5))
  
# fill with 1 the alternate rows and columns
mat[1::2, ::2] = 255
mat[::2, 1::2] = 255
      
plt.imshow(mat, cmap='gray' ,vmin=0, vmax=1)
plt.show()

    
