import matplotlib.pyplot as plt
import numpy as np
import skimage.io
from sympy import Range

img = skimage.io.imread('tiger.png', as_gray=True)
img_copy = img.copy()
h,w = img.shape

for i in range (h):
    for j in range(w):
        if img[i,j]<=210:
            img[i,j]+=45
        else:
            img[i,j]=255

img_rotated=np.zeros((w,h))
img_rotated2=np.zeros((w,h))
img_mirror=np.zeros((h,w))
img_rezolucija=np.zeros((h,w))
img_dio=np.zeros((h,w))

for i in range (h):
    for j in range(w):
        img_rotated[j,h-i-1]=img_copy[i,j]

for i in range (h):
    for j in range(w):
        img_rotated2[w-j-1,i]=img_copy[i,j]

for i in range (h):
    for j in range(w):
        img_mirror[i,w-j-1]=img_copy[i,j]

img_rezolucija=img[0:-1:10,0:-1:10] #start,end,step

for i in range (h):
    for j in Range(240,480):
        img_dio[i,j]=img_copy[i,j]

#plt.figure(1)
#plt.imshow(img, cmap='gray' ,vmin=0, vmax=255)
plt.figure(2)
plt.imshow(img_copy, cmap='gray' ,vmin=0, vmax=255)
#plt.figure(3)
#plt.imshow(img_rotated, cmap='gray' ,vmin=0, vmax=255)
#plt.figure(4)
#plt.imshow(img_mirror, cmap='gray' ,vmin=0, vmax=255)
#plt.figure(5)
#plt.imshow(img_rezolucija, cmap='gray' ,vmin=0, vmax=255)
plt.figure(6)
plt.imshow(img_dio, cmap='gray' ,vmin=0, vmax=255)
plt.show()
