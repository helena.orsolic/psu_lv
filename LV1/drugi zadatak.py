'''Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja nekakvu ocjenu i
nalazi se između 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju sljedećih uvjeta:'''

broj = input('Unesite broj od 0.0 do 1.0: ')

try:
    broj = float(broj)
except():
    print('Pogreska pri unosu')
    exit()


if 0.9 <= broj < 1.0:
    print('Ocjena A')
elif 0.8 <= broj < 0.9:
    print('Ocjena B')
elif 0.7 <= broj < 0.8:
    print('Ocjena C')
elif 0.6 <= broj < 0.7:
    print('Ocjena D')
elif broj < 0.6:
    print('Ocjena F')
else:
    print("Krivi interval")


    
    
