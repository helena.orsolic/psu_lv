from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import skimage 

#cetvrti
imageNew = mpimg.imread('example_grayscale.png')
X = imageNew.reshape((-1, 1))

plt.figure(1)
plt.title('Originalna slika')
plt.imshow(imageNew,  cmap='gray')

k_means = cluster.KMeans(n_clusters=2,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
image_compressed = np.choose(labels, values)
image_compressed.shape = imageNew.shape

skimage.io.imsave("rez.png", image_compressed)
plt.figure(2)
plt.imshow(image_compressed,  cmap='gray')
plt.show()