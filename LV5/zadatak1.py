from turtle import color
from sklearn import datasets
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                    centers=4,
                                    cluster_std=[1.0, 2.5, 0.5, 3.0],
                                    random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X


#prvi
X = generate_data(100,3)
plt.figure(1)
plt.scatter(X[:,0], X[:,1])

model = KMeans(n_clusters=3)
model.fit(X)
b=model.predict(X)
#plt.figure(2)
#plt.scatter(X[:,0], X[:,1], c=b)
#plt.scatter(model.cluster_centers_[:,0], model.cluster_centers_[:,1], color ='red', marker = 'X')

#drugi
inertias = []

for clusters in range(2,21):
     kmeans = KMeans(clusters).fit(X)
     inertias.append(kmeans.inertia_)
    
#plt.figure(3)
#plt.xlabel('Broj clustera')
#plt.ylabel('Kriterijska funkcija')
#plt.plot(range(2,21), inertias, 'bx-')


#treci
A = linkage(X, method = 'single')
B = linkage(X, method = 'complete')
C = linkage(X, method = 'average')
D = linkage(X, method = 'weighted')
E = linkage(X, method = 'ward')
plt.figure(4)
dendrogram(C)

plt.show()